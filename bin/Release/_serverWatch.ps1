cls
Write-Host "Checking OstroRis_Server.exe"

$TimeInterval = [DateTime]::Now.AddMinutes(-15)
[object] $logFile = Get-ChildItem * -include OstroRis_Server.exe.log | where {$TimeInterval -le $_.LastWriteTime}


if ($logFile.Name -eq $null) {
	Write-Host "                                                            [ERROR]"
	Write-Host ""
	
	Write-Host "Killing OstroRis_Server.exe"
	Stop-Process -name OstroRis_Server -ErrorAction silentlycontinue
	if ($? -eq $true) {
		Write-Host "                                                            [OK]"
	} else {
		Write-Host "                                                            [NOT RUNNING]"
	}
	

	Write-Host "Sending EMAIL"
	$SmtpServer = "mail-server.radio.cmm"
	$From = "OstroRisServer@radiocity.cz"
	$To = "admin@radiocity.cz"
	$Title = "OstroRIS Server byl nasilne restartovan!"
	$Body = Get-Content -path OstroRis_Server.exe.log
	$SmtpClient = new-object system.net.mail.smtpClient
	$SmtpClient.host = $SmtpServer
	$SmtpClient.Send($from, $to, $title, $Body)
	if ($? -eq $true) {
		Write-Host "                                                            [OK]"
	} else {
		Write-Host "                                                            [ERROR]"
	}
	
	
	
	Write-Host "Starting OstroRis_Server.exe"
	./OstroRis_Server.exe
	if ($? -eq $true) {
		Write-Host "                                                            [OK]"
	} else {
		Write-Host "                                                            [ERROR]"
	}
	
	
} else {
	Write-Host "                                                            [OK]"
}