dpl	0011	5	sou	371	20060822	04:00
Souhrn;dom�c�;04:00
Souhrn zpr�v �TK z domova 22. srpna 04:00
    Praha - O vzniku a fungov�n� sv� vl�dy chce p�edseda ODS a premi�r Mirek
Topol�nek jednat se soci�ln�mi demokraty maxim�ln� do konce p��t�ho t�dne.
"Ten term�n je konec p��t�ho t�dne, kdy chci prezidentu V�clavu Klausovi
p�edstavit svou vl�du, s n� p�jdu sestavovat programov� prohl�en� a ��dat
o d�v�ru v Poslaneck� sn�movn�," �ekl Topol�nek v rozhovoru pro Mladou
frontu Dnes.
    
    Praha - Do konce z��� dostanou pacienti poprv� informace, podle kter�ch si
budou moci vybrat, do kter� nemocnice p�jdou na pl�novanou operaci.
"Dostanou k dispozici skute�n� relevantn� data o kvalit�," �ekl
zpravodajsk�mu serveru Aktu�ln�.cz dosluhuj�c� ministr zdravotnictv� David
Rath (�SSD). Podle serveru by se jednalo o p�elomovou ud�lost, proto�e
dosud se v �esku ��dn� ministr k posuzov�n� kvality p��e neodhodlal.
    
    Praha - V��et p�edstavitel�, jejich slu�ebn� automobily poru�ily dopravn�
p�edpisy, roz���ili mimo jin� p�edseda Poslaneck� sn�movny Miloslav Vl�ek
(�SSD) a tajemn�k prezidenta republiky Ladislav Jakl. Mlad� fronta Dnes
(MfD) p�e, �e v pond�l�, kdy� se u budovy �esk�ho rozhlasu ve Vinohradsk�
ulici v Praze ��astnili pietn�ho aktu k v�ro�� ud�lost� v srpnu 1968, st�ly
jejich slu�ebn� vozy na z�kazu zastaven�. Podle MfD st�lo na m�st� z�kazu
zastaven� minim�ln� osm limuz�n. Jejich �idi��m by mohly za poru�en�
dopravn�ch p�edpis� hrozit ztr�ta jednoho bodu a pokuta 2000 korun.
    
    Praha - Rozs�hl� testov�n� znalost� 49.000 ��k� dev�t�ch t��d z�kladn�ch
�kol uk�zalo, �e nejlep�� po�t��i vyr�staj� na Kr�lov�hradecku a Zl�nsku,
nejhor�� naopak na �stecku. S mate�sk�m jazykem si dok�ou nejsn�ze poradit
d�ti z Liberecka, nejh��e z �stecka. K t�mto v�sledk�m dosp�lo testov�n�
d�t�, jeho� krajsk� z�v�ry zve�ejnilo Centrum pro zji��ov�n� v�sledk�
vzd�l�v�n�. P�e to dne�n� Mlad� fronta Dnes. Jedin� kraj, kter� se
testov�n� nez��astnil, byla Praha. Projekt byl toti� placen� z evropsk�ch
dotac�, na kter� nem� Praha n�rok.
    
    Praha - Man�elka premi�ra a p�edsedy ODS Mirka Topol�nka Pavla Topol�nkov�
po��dala zhruba p�ed t�emi t�dny o rozvod. S odvol�n�m na vyj�d�en� �lov�ka
bl�zk�ho ��fovi ODS to p�e dne�n� Blesk.
    
    Turnov (Semilsko) - Vlakov� turn� skupiny �echomor za�alo v pond�l� ve�er
koncertem na n�dra�� v Turnov� na Semilsku. Byl spojen se k�tem nov�ho alba
a DVD nazvan�ho Stalo sa �iv�. Hosty kapely byli zp�va�ka Lenka Dusilov� a
japonsk� hr�� na fl�tnu �akuha�i a bubny taiko D�od�i Hirota. P�ed t�m
vystoupila sedmi�lenn� slovensk� skupina Dru�ina z Bansk� Bystrice, kter�
podobn� jako �echomor hraje p�sn� vych�zej�c� z folkloru.
    
    Praha - Ma�karn� pr�vod centrem Prahy, dv� p�edstaven� a koncert zah�jily v
pond�l� odpoledne a ve�er t�et� ro�n�k mezin�rodn�ho divadeln�ho festivalu
Letn� Letn�. A� do 3. z��� se v prostoru mezi Hanavsk�m pavilonem a
Kram��ovou vilou bude konat p�ehl�dka takzvan�ho nov�ho cirkusu, pohybov�ho
divadla, klaunerie a vizu�ln�ho um�n�. Jak ale nazna�il odpoledn� pr�vod,
festival se letos rozroste i do centra Prahy.
    
    �TK
