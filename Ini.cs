using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using CodeProject.Chidi.Cryptography;

namespace Ini
{
	/// <summary>
	/// Create a New INI file to store or load data
	/// </summary>
	public class IniFile
	{
		public string path;
        private string enc_password = "68^$#@eadsDSA2qD@!DAe1eda3@!";

		[DllImport("kernel32")]
		private static extern long WritePrivateProfileString(string section,string key,string val,string filePath);
		[DllImport("kernel32")]
		private static extern int GetPrivateProfileString(string section,string key,string def,StringBuilder retVal,int size,string filePath);

		/// <summary>
		/// INIFile Constructor.
		/// </summary>
		/// <param name="INIPath"></param>
		public IniFile(string INIPath)
		{
			path = INIPath;
		}
		/// <summary>
		/// Write Data to the INI File
		/// </summary>
		/// <param name="Section"></param>
		/// Section name
		/// <param name="Key"></param>
		/// Key Name
		/// <param name="Value"></param>
		/// Value Name
		public void IniWriteValue(string Section,string Key,string Value)
		{
			WritePrivateProfileString(Section,Key,Value,this.path);
		}
		
		/// <summary>
		/// Read Data Value From the Ini File
		/// </summary>
		/// <param name="Section"></param>
		/// <param name="Key"></param>
		/// <param name="Path"></param>
		/// <returns></returns>
		public string IniReadValue(string Section,string Key)
		{
			StringBuilder temp = new StringBuilder(255);
			int i = GetPrivateProfileString(Section,Key,"",temp,255,this.path);
			return temp.ToString();

		}

        /// <summary>
        /// Read Encrypted Data Value From the Ini File
        /// </summary>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public string IniReadEncValue(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp, 255, this.path);
            try
            {
                SymCryptography cryptic = new SymCryptography();
                cryptic = new SymCryptography("TripleDES");
                cryptic.Key = this.enc_password;
                return cryptic.Decrypt(temp.ToString());
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// Write Encrypted Data to the INI File
        /// </summary>
        /// <param name="Section"></param>
        /// Section name
        /// <param name="Key"></param>
        /// Key Name
        /// <param name="Value"></param>
        /// Value Name
        public void IniWriteEncValue(string Section, string Key, string Value)
        {
            SymCryptography cryptic = new SymCryptography();
            cryptic = new SymCryptography("TripleDES");
            cryptic.Key = this.enc_password;
            Value = cryptic.Encrypt(Value);
            WritePrivateProfileString(Section, Key, Value, this.path);
        }
	}
}
