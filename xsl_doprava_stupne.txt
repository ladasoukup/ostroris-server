﻿<?xml version="1.0" encoding="UTF-8"?>
<html xsl:version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
	<body style="background-color: #BBD9FF; margin: 0px; padding: 0px;">
		<table border="0" cellspacing="0" cellpadding="0">
<xsl:for-each select="xml/data/segment">
<xsl:sort select="event/old/@degree" data-type="text" order="descending"/>
<xsl:sort select="location/element/@name" data-type="text"/>
			<tr>
				<td style="font-family: #TEXT-FAMILY#; font-size: #TEXT-SIZE#pt; border-bottom: 1px solid #111111; white-space: nowrap; text-align: center;">
					<xsl:choose>
						<xsl:when test="event/old/@degree='1'">#DOPRAVA-DEGREE-1#</xsl:when>
						<xsl:when test="event/old/@degree='2'">#DOPRAVA-DEGREE-2#</xsl:when>
						<xsl:when test="event/old/@degree='2-3'">#DOPRAVA-DEGREE-3#</xsl:when>
						<xsl:when test="event/old/@degree='3'">#DOPRAVA-DEGREE-3#</xsl:when>
						<xsl:when test="event/old/@degree='3-4'">#DOPRAVA-DEGREE-4#</xsl:when>
						<xsl:when test="event/old/@degree='4'">#DOPRAVA-DEGREE-4#</xsl:when>
						<xsl:when test="event/old/@degree='4-5'">#DOPRAVA-DEGREE-5#</xsl:when>
						<xsl:when test="event/old/@degree='5'">#DOPRAVA-DEGREE-5#</xsl:when>
						<xsl:otherwise>#DOPRAVA-DEGREE-1#</xsl:otherwise>
					</xsl:choose>
						<span style="padding: 5px; 0px;"><xsl:value-of select="event/old/@degree"/></span>
					#DOPRAVA-DEGREE-END#
				</td>
				<td style="font-family: #TEXT-FAMILY#; font-size: #TEXT-SIZE#pt; border-bottom: 1px solid #111111; white-space: nowrap;">&#160;&#160;</td>
				<td style="font-family: #TEXT-FAMILY#; font-size: #TEXT-SIZE#pt; border-bottom: 1px solid #111111; padding-right: 5px; white-space: nowrap;">
					<xsl:value-of select="location/element/@name"/>
				</td>
			</tr>
</xsl:for-each>
		</table>
	</body>
</html>