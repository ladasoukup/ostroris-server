/*
 * Created by SharpDevelop.
 * User: Soukup
 * Date: 22.8.2005
 * Time: 18:03
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using System.Windows.Forms;

namespace OstroRis_server
{
	/// <summary>
	/// Description of cfg.
	/// </summary>
	public class cfg : System.Windows.Forms.Form
	{
		private System.Windows.Forms.NumericUpDown cfg_CTK_refresh;
		private System.Windows.Forms.TextBox cfg_SQL_db_table;
		private System.Windows.Forms.TextBox cfg_SQL_db;
		private System.Windows.Forms.TextBox cfg_FTP_user;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.CheckBox cfg_FTP_pasv;
		private System.Windows.Forms.TextBox cfg_FTP_pass;
		private System.Windows.Forms.TextBox cfg_SQL_host;
		private System.Windows.Forms.TextBox cfg_SQL_user;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox cfg_FTP_host;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Button btn_save;
		private System.Windows.Forms.TextBox cfg_SQL_pass;
        private GroupBox groupBox4;
        private TextBox cfg_smtp_to;
        private Label label12;
        private TextBox cfg_smtp_from;
        private Label label11;
        private TextBox cfg_smtp_server;
        private Label label10;
        private NumericUpDown FTP_MaxErrorCount;
        private Label label13;
        private NumericUpDown MySQL_MaxErrorCount;
        private Label label14;
        private NumericUpDown MaxDataDelay_night;
        private NumericUpDown MaxDataDelay_day;
        private Label label17;
        private Label label16;
        private Label label15;
		private MainForm opener;
		public cfg(MainForm sender)
		{
			InitializeComponent();
			opener = sender;
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cfg));
            this.cfg_SQL_pass = new System.Windows.Forms.TextBox();
            this.btn_save = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cfg_FTP_host = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cfg_SQL_user = new System.Windows.Forms.TextBox();
            this.cfg_SQL_host = new System.Windows.Forms.TextBox();
            this.cfg_FTP_pass = new System.Windows.Forms.TextBox();
            this.cfg_FTP_pasv = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cfg_FTP_user = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cfg_SQL_db_table = new System.Windows.Forms.TextBox();
            this.cfg_SQL_db = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.MaxDataDelay_night = new System.Windows.Forms.NumericUpDown();
            this.MaxDataDelay_day = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.MySQL_MaxErrorCount = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.FTP_MaxErrorCount = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.cfg_CTK_refresh = new System.Windows.Forms.NumericUpDown();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cfg_smtp_to = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cfg_smtp_from = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cfg_smtp_server = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MaxDataDelay_night)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxDataDelay_day)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MySQL_MaxErrorCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FTP_MaxErrorCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cfg_CTK_refresh)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // cfg_SQL_pass
            // 
            this.cfg_SQL_pass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cfg_SQL_pass.Location = new System.Drawing.Point(104, 72);
            this.cfg_SQL_pass.Name = "cfg_SQL_pass";
            this.cfg_SQL_pass.PasswordChar = '#';
            this.cfg_SQL_pass.Size = new System.Drawing.Size(160, 21);
            this.cfg_SQL_pass.TabIndex = 13;
            // 
            // btn_save
            // 
            this.btn_save.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_save.Location = new System.Drawing.Point(286, 263);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(272, 23);
            this.btn_save.TabIndex = 3;
            this.btn_save.Text = "Uložit nastavení";
            this.btn_save.Click += new System.EventHandler(this.btn_save_click);
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(8, 120);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 24);
            this.label8.TabIndex = 16;
            this.label8.Text = "Tabulka:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(8, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(200, 24);
            this.label9.TabIndex = 0;
            this.label9.Text = "Interval stahování (s):";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cfg_FTP_host
            // 
            this.cfg_FTP_host.Location = new System.Drawing.Point(104, 24);
            this.cfg_FTP_host.Name = "cfg_FTP_host";
            this.cfg_FTP_host.Size = new System.Drawing.Size(160, 20);
            this.cfg_FTP_host.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(8, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 24);
            this.label4.TabIndex = 10;
            this.label4.Text = "Heslo:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(8, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 24);
            this.label5.TabIndex = 9;
            this.label5.Text = "Uživatel:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(8, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 24);
            this.label6.TabIndex = 8;
            this.label6.Text = "Server:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(8, 96);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 24);
            this.label7.TabIndex = 14;
            this.label7.Text = "Databáze:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(8, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Server:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(8, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Uživatel:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(8, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 24);
            this.label3.TabIndex = 2;
            this.label3.Text = "Heslo:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cfg_SQL_user
            // 
            this.cfg_SQL_user.Location = new System.Drawing.Point(104, 48);
            this.cfg_SQL_user.Name = "cfg_SQL_user";
            this.cfg_SQL_user.Size = new System.Drawing.Size(160, 20);
            this.cfg_SQL_user.TabIndex = 12;
            // 
            // cfg_SQL_host
            // 
            this.cfg_SQL_host.Location = new System.Drawing.Point(104, 24);
            this.cfg_SQL_host.Name = "cfg_SQL_host";
            this.cfg_SQL_host.Size = new System.Drawing.Size(160, 20);
            this.cfg_SQL_host.TabIndex = 11;
            // 
            // cfg_FTP_pass
            // 
            this.cfg_FTP_pass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cfg_FTP_pass.Location = new System.Drawing.Point(104, 72);
            this.cfg_FTP_pass.Name = "cfg_FTP_pass";
            this.cfg_FTP_pass.PasswordChar = '#';
            this.cfg_FTP_pass.Size = new System.Drawing.Size(160, 21);
            this.cfg_FTP_pass.TabIndex = 6;
            // 
            // cfg_FTP_pasv
            // 
            this.cfg_FTP_pasv.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cfg_FTP_pasv.Location = new System.Drawing.Point(16, 96);
            this.cfg_FTP_pasv.Name = "cfg_FTP_pasv";
            this.cfg_FTP_pasv.Size = new System.Drawing.Size(240, 24);
            this.cfg_FTP_pasv.TabIndex = 7;
            this.cfg_FTP_pasv.Text = "Použít PASIVNÍ režim přenosu.";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cfg_FTP_pasv);
            this.groupBox1.Controls.Add(this.cfg_FTP_pass);
            this.groupBox1.Controls.Add(this.cfg_FTP_user);
            this.groupBox1.Controls.Add(this.cfg_FTP_host);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.Location = new System.Drawing.Point(8, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(272, 128);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "FTP";
            // 
            // cfg_FTP_user
            // 
            this.cfg_FTP_user.Location = new System.Drawing.Point(104, 48);
            this.cfg_FTP_user.Name = "cfg_FTP_user";
            this.cfg_FTP_user.Size = new System.Drawing.Size(160, 20);
            this.cfg_FTP_user.TabIndex = 5;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.cfg_SQL_db_table);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.cfg_SQL_db);
            this.groupBox2.Controls.Add(this.cfg_SQL_host);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.cfg_SQL_user);
            this.groupBox2.Controls.Add(this.cfg_SQL_pass);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox2.Location = new System.Drawing.Point(8, 134);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(272, 152);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "MySQL";
            // 
            // cfg_SQL_db_table
            // 
            this.cfg_SQL_db_table.Location = new System.Drawing.Point(104, 120);
            this.cfg_SQL_db_table.Name = "cfg_SQL_db_table";
            this.cfg_SQL_db_table.Size = new System.Drawing.Size(160, 20);
            this.cfg_SQL_db_table.TabIndex = 17;
            // 
            // cfg_SQL_db
            // 
            this.cfg_SQL_db.Location = new System.Drawing.Point(104, 96);
            this.cfg_SQL_db.Name = "cfg_SQL_db";
            this.cfg_SQL_db.Size = new System.Drawing.Size(160, 20);
            this.cfg_SQL_db.TabIndex = 15;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.MaxDataDelay_night);
            this.groupBox3.Controls.Add(this.MaxDataDelay_day);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.MySQL_MaxErrorCount);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.FTP_MaxErrorCount);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.cfg_CTK_refresh);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox3.Location = new System.Drawing.Point(286, 102);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(272, 155);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Obecné";
            // 
            // MaxDataDelay_night
            // 
            this.MaxDataDelay_night.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.MaxDataDelay_night.Location = new System.Drawing.Point(214, 126);
            this.MaxDataDelay_night.Maximum = new decimal(new int[] {
            240,
            0,
            0,
            0});
            this.MaxDataDelay_night.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.MaxDataDelay_night.Name = "MaxDataDelay_night";
            this.MaxDataDelay_night.Size = new System.Drawing.Size(48, 20);
            this.MaxDataDelay_night.TabIndex = 10;
            this.MaxDataDelay_night.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // MaxDataDelay_day
            // 
            this.MaxDataDelay_day.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.MaxDataDelay_day.Location = new System.Drawing.Point(102, 126);
            this.MaxDataDelay_day.Maximum = new decimal(new int[] {
            240,
            0,
            0,
            0});
            this.MaxDataDelay_day.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.MaxDataDelay_day.Name = "MaxDataDelay_day";
            this.MaxDataDelay_day.Size = new System.Drawing.Size(48, 20);
            this.MaxDataDelay_day.TabIndex = 9;
            this.MaxDataDelay_day.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label17.Location = new System.Drawing.Point(168, 124);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(40, 24);
            this.label17.TabIndex = 8;
            this.label17.Text = "noc:";
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label16.Location = new System.Drawing.Point(53, 124);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 24);
            this.label16.TabIndex = 7;
            this.label16.Text = "den:";
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label15.Location = new System.Drawing.Point(6, 99);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(256, 24);
            this.label15.TabIndex = 6;
            this.label15.Text = "Max. prodleva mezi daty od ČTK";
            // 
            // MySQL_MaxErrorCount
            // 
            this.MySQL_MaxErrorCount.Location = new System.Drawing.Point(214, 76);
            this.MySQL_MaxErrorCount.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.MySQL_MaxErrorCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MySQL_MaxErrorCount.Name = "MySQL_MaxErrorCount";
            this.MySQL_MaxErrorCount.Size = new System.Drawing.Size(48, 20);
            this.MySQL_MaxErrorCount.TabIndex = 5;
            this.MySQL_MaxErrorCount.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label14.Location = new System.Drawing.Point(8, 72);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(200, 24);
            this.label14.TabIndex = 4;
            this.label14.Text = "Max. počet chyb MySQL:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // FTP_MaxErrorCount
            // 
            this.FTP_MaxErrorCount.Location = new System.Drawing.Point(214, 50);
            this.FTP_MaxErrorCount.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.FTP_MaxErrorCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.FTP_MaxErrorCount.Name = "FTP_MaxErrorCount";
            this.FTP_MaxErrorCount.Size = new System.Drawing.Size(48, 20);
            this.FTP_MaxErrorCount.TabIndex = 3;
            this.FTP_MaxErrorCount.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label13.Location = new System.Drawing.Point(8, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(200, 24);
            this.label13.TabIndex = 2;
            this.label13.Text = "Max. počet chyb FTP:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cfg_CTK_refresh
            // 
            this.cfg_CTK_refresh.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.cfg_CTK_refresh.Location = new System.Drawing.Point(214, 24);
            this.cfg_CTK_refresh.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
            this.cfg_CTK_refresh.Minimum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.cfg_CTK_refresh.Name = "cfg_CTK_refresh";
            this.cfg_CTK_refresh.Size = new System.Drawing.Size(48, 20);
            this.cfg_CTK_refresh.TabIndex = 1;
            this.cfg_CTK_refresh.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cfg_smtp_to);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.cfg_smtp_from);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.cfg_smtp_server);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox4.Location = new System.Drawing.Point(286, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(272, 96);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "SMTP";
            // 
            // cfg_smtp_to
            // 
            this.cfg_smtp_to.Location = new System.Drawing.Point(102, 68);
            this.cfg_smtp_to.Name = "cfg_smtp_to";
            this.cfg_smtp_to.Size = new System.Drawing.Size(160, 20);
            this.cfg_smtp_to.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(8, 68);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 24);
            this.label12.TabIndex = 9;
            this.label12.Text = "Příjemce:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cfg_smtp_from
            // 
            this.cfg_smtp_from.Location = new System.Drawing.Point(102, 42);
            this.cfg_smtp_from.Name = "cfg_smtp_from";
            this.cfg_smtp_from.Size = new System.Drawing.Size(160, 20);
            this.cfg_smtp_from.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.Location = new System.Drawing.Point(6, 42);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 24);
            this.label11.TabIndex = 7;
            this.label11.Text = "Odesílatel:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cfg_smtp_server
            // 
            this.cfg_smtp_server.Location = new System.Drawing.Point(102, 16);
            this.cfg_smtp_server.Name = "cfg_smtp_server";
            this.cfg_smtp_server.Size = new System.Drawing.Size(160, 20);
            this.cfg_smtp_server.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(6, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 24);
            this.label10.TabIndex = 5;
            this.label10.Text = "Server:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cfg
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(566, 293);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "cfg";
            this.Text = "OstroRIS server - nastavení";
            this.Load += new System.EventHandler(this.CfgLoad);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MaxDataDelay_night)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxDataDelay_day)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MySQL_MaxErrorCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FTP_MaxErrorCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cfg_CTK_refresh)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion
		void CfgLoad(object sender, System.EventArgs e)
		{
			string S;
			
			cfg_FTP_host.Text = opener.ini.IniReadValue("FTP", "host");
			cfg_FTP_user.Text = opener.ini.IniReadValue("FTP", "user");
			if (opener.ini.IniReadValue("FTP", "mode") == "PASV") {
				cfg_FTP_pasv.Checked = true;
			}
			cfg_SQL_host.Text = opener.ini.IniReadValue("SQL", "sql_host");
			cfg_SQL_user.Text = opener.ini.IniReadValue("SQL", "sql_user");
			cfg_SQL_db.Text = opener.ini.IniReadValue("SQL", "sql_db");
			cfg_SQL_db_table.Text = opener.ini.IniReadValue("SQL", "sql_db_table");
			S = opener.ini.IniReadValue("CTK", "refresh");
			if (S != ""){
				cfg_CTK_refresh.Value = Convert.ToInt32(S);
			} else {
				cfg_CTK_refresh.Value = 120;
			}

            cfg_smtp_server.Text = opener.ini.IniReadValue("smtp", "MailServer");
            cfg_smtp_to.Text = opener.ini.IniReadValue("smtp", "MailTo");
            cfg_smtp_from.Text = opener.ini.IniReadValue("smtp", "MailFrom");

            S = opener.ini.IniReadValue("ReportError", "FTP");
            if (S != "") FTP_MaxErrorCount.Value = Convert.ToInt32(S);
            S = opener.ini.IniReadValue("ReportError", "MySQL");
            if (S != "") MySQL_MaxErrorCount.Value = Convert.ToInt32(S);
            S = opener.ini.IniReadValue("ReportError", "Delay-night");
            if (S != "") MaxDataDelay_night.Value = Convert.ToInt32(S);
            S = opener.ini.IniReadValue("ReportError", "Delay-day");
            if (S != "") MaxDataDelay_day.Value = Convert.ToInt32(S);

		}
		
		void btn_save_click(object sender, System.EventArgs e)
		{
			opener.ini.IniWriteValue("FTP", "host", cfg_FTP_host.Text);
			opener.ini.IniWriteValue("FTP", "user", cfg_FTP_user.Text);
            if (cfg_FTP_pass.Text != "")
            {
                opener.ini.IniWriteEncValue("FTP", "pass", cfg_FTP_pass.Text);
            }
                if (cfg_FTP_pasv.Checked == true){
				opener.ini.IniWriteValue("FTP", "mode", "PASV");
			} else {
				opener.ini.IniWriteValue("FTP", "mode", "");
			}
			
			opener.ini.IniWriteValue("SQL", "sql_host", cfg_SQL_host.Text);
			opener.ini.IniWriteValue("SQL", "sql_user", cfg_SQL_user.Text);
            if (cfg_SQL_pass.Text != "")
            {
                opener.ini.IniWriteEncValue("SQL", "sql_pass", cfg_SQL_pass.Text);
            }
			opener.ini.IniWriteValue("SQL", "sql_db", cfg_SQL_db.Text);
			opener.ini.IniWriteValue("SQL", "sql_db_table", cfg_SQL_db_table.Text);
			
			opener.ini.IniWriteValue("CTK", "refresh", cfg_CTK_refresh.Value.ToString());

            opener.ini.IniWriteValue("smtp", "MailServer", cfg_smtp_server.Text);
            opener.ini.IniWriteValue("smtp", "MailTo", cfg_smtp_to.Text);
            opener.ini.IniWriteValue("smtp", "MailFrom", cfg_smtp_from.Text);

            opener.ini.IniWriteValue("ReportError", "FTP", FTP_MaxErrorCount.Value.ToString());
            opener.ini.IniWriteValue("ReportError", "MySQL", MySQL_MaxErrorCount.Value.ToString());
            opener.ini.IniWriteValue("ReportError", "Delay-night", MaxDataDelay_night.Value.ToString());
            opener.ini.IniWriteValue("ReportError", "Delay-day", MaxDataDelay_day.Value.ToString());

            this.Close();
		}
	}
}
