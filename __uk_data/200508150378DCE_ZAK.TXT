dce	0648	4	zak	133	20060220	20:39
�R-policie-person�ln�-Hus�k                             
�T: Kandid�t na policejn�ho prezidenta Hus�k tajil nehodu �idi�e
    PRAHA 15. srpna (�TK) - N�m�stek policejn�ho prezidenta pro
uniformovanou policii Vladislav Hus�k, kter� je v�n�m kandid�tem
na nov�ho policejn�ho prezidenta, cht�l p�ed dv�ma lety zatajit
nehodu sv�ho �idi�e. Mu� po hav�rii na pra�sk� magistr�le odm�tl
dechovou zkou�ku. Hus�k, jen� p�i nehod� autem necestoval,
po��dal dopravn� dispe�ink, aby se o kolizi nedozv�d�l policejn�
prezident Ji�� Kol��. Uvedla to dnes �esk� televize.
    Z�znam Hus�kova telefon�tu s dispe�inkem, v n�m� Hus�k ��d�,
aby o nehod� nebyl informov�n "kn�ra�", Kol�� nicm�n� z�skal. "To
m� znepokojilo a taky jsme si to okam�it� vy��kali," �ekl Kol��
�T. Hus�ka tehdy nijak nepotrestal, prosadil v�ak, aby byl od
policie propu�t�n jeho �idi�. Hus�k uvedl, �e si obsah sv�ho
telefon�tu s dopravn�m dispe�inkem podrobn� nepamatuje.
    Ministru vnitra Franti�ku Bublanovi dva roky star� kauza
nevad�, nebo� Hus�k u nehody p��mo nebyl. Jin� postoj zast�v�
m�stop�edseda ODS a st�nov� ministr vnitra Ivan Langer. "Pokud by
se uk�zalo, �e k n��emu takov�mu do�lo, vn�mal bych to jako v�n�
probl�m," �ekl.
    Kol�� dne�n�m dnem ve funkci kon��. Zast�val ji do sv�
�ervnov� rezignace t�m�� sedm let a stal se tak zat�m nejd�le
slou��c�m polistopadov�m policejn�m ��fem. Bublan dosud
neozn�mil, kdo Kol��e v nejvy��� policejn� funkci nahrad�.
Favoritem je pr�v� Hus�k.
    khj rot
