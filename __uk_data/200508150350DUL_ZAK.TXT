dul	0606	4	zak kul pol	152	20060220	19:24
�R-policie-hudba-techno-CzechTek-protest-�st� n. L.     
V �st� protestovalo proti z�sahu policie na CzechTeku na 200 lid�
    �ST� NAD LABEM 15. srpna (�TK) - Zhruba 200 p�ev�n� mlad�ch
lid� protestovalo dnes v �st� nad Labem proti z�sahu policie na
CzechTeku. Na shrom�d�n�, kter� bylo ��dn� ohl�eno,
podepisovali n�kte�� jeho ��astn�ci petici na obranu lidsk�ch
pr�v, n�kte�� jen poslouchali muziku, v�t�ina z nich se pak
vyst��dala u po��ta�ov�ch monitor�, na kter�ch b�ely z�b�ry z
policejn�ho z�sahu u Ml�nce na Tachovsku.
    Vladim�r Charv�t, kter� byl jedn�m z po�adatel� shrom�d�n�,
�TK �ekl, �e �ste�t� odp�rci policejn�ho z�sahu jezdili v
posledn�ch dnech hodn� na protestn� akce do Prahy, proto se v
krajsk�m m�st� se�li a� dnes.
    Podle n�j na protest nen� pozd�, i kdy� od z�sahu uplynuly
v�ce ne� dva t�dny. "Nic se nesm� ututlat," zd�raznil Charv�t.
Proto podle n�j budou protesty v �st� pokra�ovat, ale budou m�t
komorn�j�� r�z. "Nechceme obt�ovat obyvatele m�sta, a tak
po��t�me nap��klad s t�m, �e po n�vratu vysoko�kol�k� z pr�zdnin
se budou diskuse konat t�eba v aule," uvedl Charv�t.
    N�kte�� z mlad�ch ��astn�k� protestu �TK �ekli, �e p�i�li
poslouchat hudbu, petici pr� podepisovat nebudou, proto�e na
CzechTeku nebyli. Dvojice star��ch lid�, kter� p�ech�zela p�es
Lidick� n�m�st�, pouze utrousila, �e by r�da podepsala petici na
podporu policejn�ho z�sahu, ale tu pr� ke �kod� spole�nosti nikdo
neorganizuje.
    Techno a hudba dal��ch styl� bude zn�t v �steck�m centru do
21:30. Na protestn� shrom�d�n� mlad�ch lid� dohl�ely po�etn�j��
hl�dky str�n�k�. Akce se konala p��mo pod okny severo�esk�
policejn� spr�vy.
    Eli�ka Cesarov� snm
