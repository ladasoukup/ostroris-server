dce	0612	4	pol	245	20060220	19:33
�R-�SSD-StB-2. VERZE                                    
�Ro: �lenem �VV �SSD je b�val� elitn� komunistick� rozv�d��k
(Ve 3. a 4. odstavci vyj�d�en� Grosse a Sobotky)
    PRAHA 15. srpna (�TK) - Jedn�m z �len� �st�edn�ho v�konn�ho
v�boru �SSD je i b�val� elitn� komunistick� rozv�d��k. Podle
zji�t�n� �esk�ho rozhlasu 1 - Radio�urn�lu se Marian Ku� p�ed
rokem 1989 infiltroval do c�rkevn�ho prost�ed� v �esku a Polsku a
rozv�dka po��tala dokonce i s jeho vysazen�m ve Vatik�nu.
    Ku� ji� d��ve Radio�urn�lu �ekl, �e nikdy s nedemokratickou
tajnou polici� nespolupracoval. Jeho tvrzen� ale definitivn�
zpochybnily d�kazy z archivu ministerstva vnitra v Pardubic�ch,
uvedl Radio�urn�l.
    P�edseda �SSD Stanislav Gross dnes novin���m �ekl, �e
soci�ln� demokrat� budou cht�t tuto v�c vyjasnit. "Bude asi
dobr�, kdy� n�m (Ku�) uk�e n�jak� �erstv� lustra�n� osv�d�en�,"
uvedl. ��astn�ci dne�n�ho jedn�n� p�edsednictva �SSD ale podle
n�j o tomto p��padu dopodrobna nejednali, a to i proto, �e Ku� na
n�m nebyl. Gross na dotaz novin��� dodal, �e Ku�e nyn� nikdo
nem��e zbavit �lenstv� v �st�edn�m v�konn�m v�boru.
    M�stop�edseda strany Bohuslav Sobotka �ekl, �e Ku� je
z�stupcem jedn� ze severomoravsk�ch okresn�ch organizac� �SSD.
"Prim�rn� o tom tak� mus� jednat soci�ln� demokrat� v tomto
okrese a v tomto regionu," uvedl.
    Ku� podle Radio�urn�lu za�al s rozv�dkou spolupracovat v
b�eznu 1986. Jeho svazek podle Radio�urn�lu ��t� za dva roky
pr�ce p�es 500 stran. Ku� se infiltroval do c�rkevn�ho prost�ed�
na severu Moravy a v Polsku a poda�ilo se mu mimo jin� nav�zat
�zk� kontakt s b�val�m kardin�lem a biskupem v Katovic�ch
Herbertem Bednorzem.
    Rozv�dka Ku�ovi um�le vytv��ela obraz re�imu nep��telsk�ho
studenta, kter� mu m�l pomoci proj�t slo�it�m s�tem v�b�ru
student� elitn�ho katolick�ho semin��e v ��m�. Ve spise je tak�
uvedeno, �e Ku� sv�m ��d�c�m d�stojn�k�m navrhoval, �e by mohl
b�t do It�lie vysl�n ne jako pouh� spolupracovn�k, ale u� jako
k�drov� d�stojn�k ministerstva vnitra.
    Proto�e se v�ak n�stup do katolick�ho semin��e oddaloval a
Ku�ova p��prava na studium v ��m� za�ala polevovat, v roce 1988
u� ��d�c� d�stojn�ci do hl�en� uvedli, �e vysl�n� do ��ma nevid�
jako p��li� re�ln�. Rozv�dka s n�m ukon�ila spolupr�ci v srpnu
1988.
    Ku��v probl�m vy�el najevo, kdy� sv� stran� p�edlo�il
negativn� lustra�n� osv�d�en�. Ministerstvo vnitra, kter� mu ale
vydalo pozitivn� osv�d�en�, na n�ho podalo trestn� ozn�men�.
Obvin�n� bylo pozd�ji zru�eno, proto�e zfal�oval jen kopii
osv�d�en�, a ne ov��enou listinu, uvedl Radio�urn�l. Ku� byl tak�
m�stostarostou �esk�ho T��na na Karvinsku, na funkci ale letos v
kv�tnu rezignoval.
    pba hj kar
