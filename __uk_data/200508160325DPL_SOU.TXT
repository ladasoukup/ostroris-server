dpl	0594	5	sou	384	20060220	20:00
SOUHRN �TK 20:00 - dom�c�                               
Souhrn zpr�v �TK z domova - 16. srpna 20:00

    PRAHA - Poslaneck� sn�movna jednohlasn� po��dala vl�du, aby
do p��t� sch�ze poslanc� p�ipravila komplexn� zpr�vu o z�sahu
policie proti ��astn�k�m technoparty CzechTek v Ml�nci na
Tachovsku. U� dnes se na toto t�ma za�alo ve sn�movn� diskutovat.
Poslanci sice souhlasili s term�nem pod�n� anal�zy, v�ce ne�
hodinovou rozpravu ale vyprovokovala slovn� p�est�elka poslance
ODS Ivana Langra a premi�ra Ji��ho Paroubka (�SSD).

    �ESK� BUD�JOVICE - Vytrval� d隝 zvedal od dne�n�ho r�na
hladiny n�kter�ch jiho�esk�ch �ek. Nejhor�� situace byla na ���ce
�ern� na �eskokrumlovsku, jej� hladina vystoupila odpoledne nad
t�et� stupe� povod�ov� aktivity. V Bene�ov� nad �ernou voda
vnikla do p�ti dom�. Jedna rodina musela sv�j domov opustit.
Dobrovoln� hasi�i evakuovali i tamn� d�tsk� t�bor. Do budovy
mate�sk� �koly p�evezli 37 lid�, p�ev�n� d�t�. Vpodve�er hladina
za�ala klesat. "B�hem hodiny a p�l voda podle na�eho m��en�
opadla o 15 centimetr�," �ekl �TK starosta obce Miloslav �im�nek.

    PRAHA - Kandid�t na ��fa ��adu vl�dy Ivan P�ikryl, o jeho�
jmenov�n� do funkce bude vl�da rozhodovat ve st�edu, dnes pop�el,
�e by spolupracoval s komunistickou St�tn� bezpe�nost� (StB). Na
mimo��dn� tiskov� konferenci prohl�sil, �e m� negativn� lustra�n�
osv�d�en�, je� musel p�edlo�it u� loni, kdy se stal n�m�stkem
ministra pro m�stn� rozvoj. P�ikryl neuva�uje o tom, �e by se o
funkci vedouc�ho ��adu vl�dy neuch�zel.

    PRAHA - V�t�zslav Jand�k nep�ich�z� na ministerstvo kultury
d�lat velk� zm�ny, nepo��t� ani se zm�nami v person�ln�m obsazen�
��adu. "Jsem klidn�, jen m�m velmi m�lo �asu do za��tku
projedn�v�n� rozpo�tu na p��t� rok," �ekl dnes v rozhovoru s
�TK. "Nejdu tam jako sekyrmajstr, kter� bude lidi vyhazovat,
mus�m ten ��ad p�evz�t. Naopak bych je cht�l zmobilizovat, aby mi
lidsky pomohli s prac� tak, jak o n� m�m p�edstavu," uvedl.

    PRAHA - Nemocnice podle v�konn�ho �editele Svazu zdravotn�ch
poji��oven Jarom�ra Gajd��ka dostanou v�ce pen�z za p��i, ne� se
p�vodn� p�edpokl�dalo, p�esto st�le na��kaj�, �e je to m�lo.
Gajd��ek to dnes �ekl �TK. P�ipomn�l, �e p�vodn� m�ly nemocnice
dostat stejn� jako loni, posl�ze bylo dohodnuto, �e platby o t�i
procenta stoupnou. "Ani to ale nemocnic�m nesta��," dodal.

    PRAHA - Protesty proti policejn�mu z�sahu na technoparty
CzechTek se nyn� p�esouvaj� z ulic do klub� a na festivaly. Dnes
se takov� "demonstrace v mal�m" uskute�nila v pra�sk�m klubu
Roxy. Bez n�roku na honor�� tu p�ed po�etn�m publikem mlad�ch
lid� hr�li r�zn� kapely a d�d�ejov�. Z�jemci mohli v klubu tak�
podepsat petici proti �dajn�mu poru�ov�n� lidsk�ch pr�v, v n� se
d�razn� protestuje proti "brut�ln�mu a neopodstatn�n�mu" z�sahu.
Podpisy rychle p�ib�valy, p�ed pultem s archy se ob�as tvo�ily i
fronty. V klubov�m prostoru NoD se prom�taly z�b�ry z policejn�
akce.


    PRAHA - �esk� spole�nost trp�v� p�edsudky v��i star��m lidem
a nen� jim v�dy p��li� p��telsky naklon�na. Ka�d� osm� �ech �i
�e�ka nad 50 let se n�kdy setkali s t�m, �e je ciz� �lov�k
po�astoval osloven�m d�de�ku �i babi�ko. Zhruba stejn� po�et lid�
si vyslechl pozn�mku, �e na ur�itou �innost jsou u� p��li� sta��.
Uv�d� to leto�n� zpr�va z v�zkumu o v�kov� diskriminaci, kterou
zve�ejnil na internetu V�zkumn� �stav pr�ce a soci�ln�ch v�c�.

    �ST� NAD LABEM - �steck� krajsk� soud dnes za�al projedn�vat
p��pad prostitutky Martiny Jandov�, kter� podle ob�aloby se sv�mi
dv�ma n�meck�mi p��teli v Krupce na Teplicku m�lem zavra�dila
�enu, u n� nab�zela sexu�ln� slu�by. Trojice loni v listopadu
napadla �enu v jej�m byt� a podle �alobce j� zasadila 13 ran
no�em. �enu zachr�nila v�asn� l�ka�sk� pomoc.

    PRAHA - Fragment sklen�n� font�ny, kter� byla sou��st� slavn�
�esk� expozice na sv�tov� v�stav� v Bruselu v roce 1958, si mohou
prohl�dnout z�jemci na v�stav� v galerii �stavu makromolekul�rn�
chemie Akademie v�d na Pet�in�ch. V�stava zahrnuje i technickou
dokumentaci k d�lu, kter� jeho autorka Dana Hlobilov� se sv�mi
kolegy vytvo�ila tehdy novou technologi� za pou�it� polymerov�ch
prysky�ic.

    �TK
