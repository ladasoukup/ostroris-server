/*
 * Created by SharpDevelop.
 * User: Soukup
 * Date: 15.8.2005
 * Time: 20:44
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using MySQLDriverCS;
using Ini;
using System.Net.Mail;
using EnterpriseDT.Net.Ftp;
using ICSharpCode.SharpZipLib.Zip;
using System.Net;
using System.Xml.XPath;
using System.Xml;
using System.Xml.Xsl;
using System.Data;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


namespace OstroRis_server
{
    /// <summary>
    /// Description of MainForm.
    /// </summary>
    public class MainForm : System.Windows.Forms.Form
    {
        private System.ComponentModel.IContainer components;
        public System.Windows.Forms.TextBox txt_debug;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_LastError;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ProgressBar ctk_progress;
        private System.Windows.Forms.Timer timer_wait;
        private System.Windows.Forms.Timer timer_DownloadCtkNews;
        private System.Windows.Forms.Button btn_cfg;
        private System.Windows.Forms.Label lbl_status;
        private System.Windows.Forms.Button btn_stop;
        private System.Windows.Forms.Button btn_DownloadCtkNews;
        // GLOBALS
        public IniFile ini = new IniFile(Application.StartupPath.ToString() + "\\OstroRis_server.ini");
        public MySQLConnection MySQL;
        public DateTime archive_ZIP_lastRun = DateTime.Now.AddDays(-1);
        public DateTime LastDownloadedData = DateTime.Now;
        public int MySQL_ErrorCount = 0;
        public int FTP_ErrorCount = 0;
        public bool ErrorReported = false;
        private StatusStrip app_status;
        private ToolStripStatusLabel app_status_lastdownload;
        private ToolStripStatusLabel app_status_FTP_error;
        private ToolStripStatusLabel toolStripStatusLabel1;
        private ToolStripStatusLabel toolStripStatusLabel2;
        private ToolStripStatusLabel toolStripStatusLabel3;
        private ToolStripStatusLabel toolStripStatusLabel4;
        private ToolStripStatusLabel app_status_MySQL_error;
        private ToolStripStatusLabel toolStripStatusLabel5;
        private ToolStripStatusLabel app_status_traffic;
        private DateTime LastDownloadedTraffic = DateTime.Now.AddMinutes(-10);
        private DateTime LastCacheUpdate = DateTime.Now.AddHours(-24);
        private DateTime LastDBcleanup = DateTime.Now.AddHours(-99);

        public MainForm()
        {
            InitializeComponent();
        }

        [STAThread]
        public static void Main(string[] args)
        {
            Application.Run(new MainForm());
        }

        #region Windows Forms Designer generated code
        /// <summary>
        /// This method is required for Windows Forms designer support.
        /// Do not change the method contents inside the source code editor. The Forms designer might
        /// not be able to load this method if it was changed manually.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btn_DownloadCtkNews = new System.Windows.Forms.Button();
            this.btn_stop = new System.Windows.Forms.Button();
            this.lbl_status = new System.Windows.Forms.Label();
            this.btn_cfg = new System.Windows.Forms.Button();
            this.timer_DownloadCtkNews = new System.Windows.Forms.Timer(this.components);
            this.timer_wait = new System.Windows.Forms.Timer(this.components);
            this.ctk_progress = new System.Windows.Forms.ProgressBar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_debug = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_LastError = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.app_status = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.app_status_FTP_error = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.app_status_MySQL_error = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.app_status_lastdownload = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel5 = new System.Windows.Forms.ToolStripStatusLabel();
            this.app_status_traffic = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1.SuspendLayout();
            this.app_status.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_DownloadCtkNews
            // 
            this.btn_DownloadCtkNews.BackColor = System.Drawing.Color.Green;
            this.btn_DownloadCtkNews.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_DownloadCtkNews.ForeColor = System.Drawing.Color.White;
            this.btn_DownloadCtkNews.Location = new System.Drawing.Point(8, 8);
            this.btn_DownloadCtkNews.Name = "btn_DownloadCtkNews";
            this.btn_DownloadCtkNews.Size = new System.Drawing.Size(144, 23);
            this.btn_DownloadCtkNews.TabIndex = 1;
            this.btn_DownloadCtkNews.Text = "STÁHNOUT OKAMŽITĚ";
            this.btn_DownloadCtkNews.UseVisualStyleBackColor = false;
            this.btn_DownloadCtkNews.Click += new System.EventHandler(this.btn_DownloadCtkNews_click);
            // 
            // btn_stop
            // 
            this.btn_stop.BackColor = System.Drawing.Color.Maroon;
            this.btn_stop.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_stop.ForeColor = System.Drawing.Color.White;
            this.btn_stop.Location = new System.Drawing.Point(160, 8);
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.Size = new System.Drawing.Size(144, 23);
            this.btn_stop.TabIndex = 5;
            this.btn_stop.Text = "ZASTAVIT SERVER";
            this.btn_stop.UseVisualStyleBackColor = false;
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_click);
            // 
            // lbl_status
            // 
            this.lbl_status.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_status.Location = new System.Drawing.Point(88, 40);
            this.lbl_status.Name = "lbl_status";
            this.lbl_status.Size = new System.Drawing.Size(496, 16);
            this.lbl_status.TabIndex = 3;
            this.lbl_status.Text = "-";
            // 
            // btn_cfg
            // 
            this.btn_cfg.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_cfg.Location = new System.Drawing.Point(480, 8);
            this.btn_cfg.Name = "btn_cfg";
            this.btn_cfg.Size = new System.Drawing.Size(104, 23);
            this.btn_cfg.TabIndex = 2;
            this.btn_cfg.Text = "NASTAVENÍ";
            this.btn_cfg.Click += new System.EventHandler(this.btn_cfg_click);
            // 
            // timer_DownloadCtkNews
            // 
            this.timer_DownloadCtkNews.Enabled = true;
            this.timer_DownloadCtkNews.Interval = 5000;
            this.timer_DownloadCtkNews.Tick += new System.EventHandler(this.timer_DownloadCtkNews_tick);
            // 
            // timer_wait
            // 
            this.timer_wait.Interval = 5000;
            this.timer_wait.Tick += new System.EventHandler(this.timer_wait_tick);
            // 
            // ctk_progress
            // 
            this.ctk_progress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ctk_progress.Location = new System.Drawing.Point(8, 56);
            this.ctk_progress.Name = "ctk_progress";
            this.ctk_progress.Size = new System.Drawing.Size(578, 24);
            this.ctk_progress.Step = 1;
            this.ctk_progress.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txt_debug);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lbl_LastError);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.Location = new System.Drawing.Point(8, 96);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(578, 286);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "LOG";
            // 
            // txt_debug
            // 
            this.txt_debug.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_debug.Location = new System.Drawing.Point(8, 31);
            this.txt_debug.Multiline = true;
            this.txt_debug.Name = "txt_debug";
            this.txt_debug.ReadOnly = true;
            this.txt_debug.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_debug.Size = new System.Drawing.Size(562, 249);
            this.txt_debug.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(8, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Poslední chyba:";
            // 
            // lbl_LastError
            // 
            this.lbl_LastError.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_LastError.Location = new System.Drawing.Point(104, 16);
            this.lbl_LastError.Name = "lbl_LastError";
            this.lbl_LastError.Size = new System.Drawing.Size(464, 16);
            this.lbl_LastError.TabIndex = 2;
            this.lbl_LastError.Text = "-";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(8, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Stav serveru:";
            // 
            // app_status
            // 
            this.app_status.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.app_status_FTP_error,
            this.toolStripStatusLabel3,
            this.app_status_MySQL_error,
            this.toolStripStatusLabel4,
            this.app_status_lastdownload,
            this.toolStripStatusLabel5,
            this.app_status_traffic});
            this.app_status.Location = new System.Drawing.Point(0, 383);
            this.app_status.Name = "app_status";
            this.app_status.Size = new System.Drawing.Size(594, 24);
            this.app_status.TabIndex = 6;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(42, 19);
            this.toolStripStatusLabel1.Text = "Chyby";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(34, 19);
            this.toolStripStatusLabel2.Text = "FTP:";
            // 
            // app_status_FTP_error
            // 
            this.app_status_FTP_error.Name = "app_status_FTP_error";
            this.app_status_FTP_error.Size = new System.Drawing.Size(13, 19);
            this.app_status_FTP_error.Text = "0";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(52, 19);
            this.toolStripStatusLabel3.Text = "MySQL:";
            // 
            // app_status_MySQL_error
            // 
            this.app_status_MySQL_error.Name = "app_status_MySQL_error";
            this.app_status_MySQL_error.Size = new System.Drawing.Size(13, 19);
            this.app_status_MySQL_error.Text = "0";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(85, 19);
            this.toolStripStatusLabel4.Text = "Poslední data:";
            // 
            // app_status_lastdownload
            // 
            this.app_status_lastdownload.Name = "app_status_lastdownload";
            this.app_status_lastdownload.Size = new System.Drawing.Size(49, 19);
            this.app_status_lastdownload.Text = "00:00:00";
            // 
            // toolStripStatusLabel5
            // 
            this.toolStripStatusLabel5.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.toolStripStatusLabel5.Name = "toolStripStatusLabel5";
            this.toolStripStatusLabel5.Size = new System.Drawing.Size(58, 19);
            this.toolStripStatusLabel5.Text = "Doprava:";
            // 
            // app_status_traffic
            // 
            this.app_status_traffic.Name = "app_status_traffic";
            this.app_status_traffic.Size = new System.Drawing.Size(49, 19);
            this.app_status_traffic.Text = "00:00:00";
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(594, 407);
            this.Controls.Add(this.app_status);
            this.Controls.Add(this.btn_stop);
            this.Controls.Add(this.btn_cfg);
            this.Controls.Add(this.ctk_progress);
            this.Controls.Add(this.btn_DownloadCtkNews);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl_status);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "OstroRIS server";
            this.Load += new System.EventHandler(this.MainFormLoad);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.app_status.ResumeLayout(false);
            this.app_status.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        void MainFormLoad(object sender, System.EventArgs e)
        {
            txt_debug.Text = Application.ProductName + " (ver. " + Application.ProductVersion.ToString() + ")";
            this.Text += " (" + Application.ProductVersion.ToString() + ")";
        }

        public void LogDebug(string dbgText, string dbgType)
        {
            // <summary>
            // Tato metoda prida text do DebugLogu :D
            // </summary>
            // TODO: Logovani do souboru (podle data)

            int TrimLength = 20000;
            if (txt_debug.Text.Length > TrimLength)
            {
                txt_debug.Text = txt_debug.Text.Substring((txt_debug.Text.Length - TrimLength), TrimLength);
            }
            string dbgTime = System.DateTime.Now.Hour.ToString("0#") + ":" + System.DateTime.Now.Minute.ToString("0#") + ":" + System.DateTime.Now.Second.ToString("0#");
            txt_debug.AppendText("\r\n" + dbgTime + "   " + dbgType + ": " + dbgText);
        }

        private void WriteAppLog()
        {
            StreamWriter LogFile = new StreamWriter(Application.ExecutablePath + ".log", false, System.Text.Encoding.GetEncoding("windows-1250"));
            LogFile.Write(txt_debug.Text);
            LogFile.Close();
        }

        public void ErrorMailSend(string msgTitle, string msgText)
        {
            string MailServer = ini.IniReadValue("smtp", "MailServer");
            string MailTo = ini.IniReadValue("smtp", "MailTo");
            string MailFrom = ini.IniReadValue("smtp", "MailFrom");

            if (MailServer != "")
            {
                try
                {
                    SmtpClient client = new SmtpClient(MailServer);
                    MailAddress from = new MailAddress(MailFrom, "OstroRIS server", System.Text.Encoding.UTF8);
                    MailAddress to = new MailAddress(MailTo);
                    MailMessage message = new MailMessage(from, to);

                    message.Subject = "OstroRIS server - " + msgTitle;

                    message.Body = msgTitle;
                    message.Body += Environment.NewLine;
                    message.Body += msgText;
                    message.Body += Environment.NewLine;
                    message.Body += System.DateTime.Now.Day.ToString("0#") + "." + System.DateTime.Now.Month.ToString("0#") + "." + System.DateTime.Now.Year.ToString("####") + " - ";
                    message.Body += System.DateTime.Now.Hour.ToString("0#") + ":" + System.DateTime.Now.Minute.ToString("0#");
                    message.Body += Environment.NewLine;
                    message.Body += Application.ProductName + ", v. " + Application.ProductVersion.ToString();

                    message.BodyEncoding = System.Text.Encoding.UTF8;
                    message.SubjectEncoding = System.Text.Encoding.UTF8;

                    client.Send(message);
                    message.Dispose();
                }
                catch
                {
                    LogDebug("Send email failed!", "ERROR");
                }
            }
        }

        public void DownloadCtkNews()
        {
            LogDebug("Downloading data from CTK server", "SYSTEM");
            string S = null;
            int I = -1;
            int ctk_refresh = 120;
            bool StartTimer = true;
            string ftp_host = "";
            string ftp_user = "";
            string ftp_pass = "";
            string ftp_mode = "";
            string sql_host = "";
            string sql_user = "";
            string sql_pass = "";
            string sql_db = "";
            string sql_db_table = "";
            FTPClient ftp = null;
            bool LoginSuc = true;
            int MySQL_MaxErrorCount = 3;
            int FTP_MaxErrorCount = 3;
            int MaxIntervalBetweenDatas = 60; // in minutes...
            MySQLCommand sql_cmd;
            MySQLDataReader sql_reader;
            string sql_query;

            lbl_status.Text = "Stahuji data ze serveru ČTK";
            lbl_LastError.Text = "-";
            timer_DownloadCtkNews.Enabled = false;
            timer_wait.Enabled = false;
            btn_DownloadCtkNews.Enabled = false;
            btn_stop.Enabled = false;

            try
            {
                //FTP connect
                ftp_host = ini.IniReadValue("FTP", "host");
                ftp_user = ini.IniReadValue("FTP", "user");
                ftp_pass = ini.IniReadEncValue("FTP", "pass");
                ftp_mode = ini.IniReadValue("FTP", "mode");

                try
                {
                    ftp = new FTPClient(ftp_host);
                    LogDebug("Connected to \"" + ftp_host + "\"", "FTP");
                }
                catch { LogDebug("Connection to \"" + ftp_host + "\" failed.", "FTP"); }
                
                try
                {
                    ftp.Login(ftp_user, ftp_pass);
                    LogDebug("User " + ftp_user + " logged in.", "FTP");
                    FTP_ErrorCount = 0;
                }
                catch
                {
                    LogDebug("User " + ftp_user + " cannot log in.", "FTP");
                    LoginSuc = false;
                    lbl_LastError.Text = "FTP - User " + ftp_user + " cannot log in.";
                    FTP_ErrorCount++;
                }


                //MySQL connect
                sql_host = ini.IniReadValue("sql", "sql_host");
                sql_user = ini.IniReadValue("sql", "sql_user");
                sql_pass = ini.IniReadEncValue("sql", "sql_pass");
                sql_db = ini.IniReadValue("sql", "sql_db");
                sql_db_table = ini.IniReadValue("sql", "sql_db_table");

                try { MySQL = new MySQLConnection(new MySQLConnectionString(sql_host, sql_db, sql_user, sql_pass).AsString); }
                catch { LogDebug("Connection to \"" + sql_host + "\" failed.", "SQL"); }
                try
                {
                    MySQL.Open();
                    LogDebug("Connected to \"" + sql_host + "\"", "SQL");
                    MySQL_ErrorCount = 0;
                }
                catch (Exception sql_e)
                {
                    LogDebug("Connection to \"" + sql_host + "\" failed.", "SQL");
                    LogDebug(sql_e.ToString(), "SQL");
                    LoginSuc = false;
                    lbl_LastError.Text = "MySQL - Connection to \"" + sql_host + "\" failed";
                    MySQL_ErrorCount++;
                }

                if (LoginSuc == true)
                {
                    //Parse NEWS - FTP
                    if (ftp_mode == "PASV")
                    {
                        LogDebug("Switching to PASV mode", "FTP");
                        ftp.ConnectMode = FTPConnectMode.PASV;
                    }
                    else
                    {
                        LogDebug("Switching to ACTIVE mode", "FTP");
                        ftp.ConnectMode = FTPConnectMode.ACTIVE;
                    }
                    ftp.TransferType = FTPTransferType.ASCII;

                    string[] files = ftp.Dir("*.TXT", false);
                    LogDebug(files.Length.ToString() + " files to take care of.", "FTP");
                    ftp.TransferType = FTPTransferType.BINARY;
                    ctk_progress.Maximum = files.Length;
                    for (int i = 0; i < files.Length; i++)
                    {
                        ctk_progress.Value = (i + 1);
                        lbl_status.Text = "Zpracovávám zprávu číslo " + (i + 1) + " z celkového počtu " + files.Length;
                        Application.DoEvents();

                        ftp.Get(Application.StartupPath + "\\temp.txt", files[i]);
                        CTK_parseTXTdata(MySQL, sql_db_table);
                        CTK_TXT_archive(files, i);

                        try
                        {
                            ftp.Delete(files[i]);
                        }
                        catch
                        {
                            // StartTimer = false; - depraceted...
                            //lbl_status.Text = "Došlo k závažné chybě! Odstraňte ji a stiskněte tlačítko \"STÁHNOU OKAMŽITĚ\".";
                            lbl_status.Text = "Došlo k závažné chybě - soubor nelze smazat z FTP!";

                            ErrorMailSend("Došlo k závažné chybě - soubor nelze smazat z FTP!", "Unable to delete file \"FTP:" + files[i] + "\"");
                            throw new InvalidOperationException("Unable to delete file \"FTP:" + files[i] + "\"");
                        }
                    }
                    lbl_status.Text = "Zprávy byly staženy se serveru.";
                }

                // PARSE NEWS - IMPORT
                if (System.IO.Directory.Exists(Application.StartupPath + "\\import"))
                {
                    string[] import_files = Directory.GetFiles(Application.StartupPath + "\\import");
                    foreach (string import_file in import_files)
                    {
                        LogDebug(import_file.Replace(Application.StartupPath+"\\", "") , "Import");
                        System.IO.File.Delete(Application.StartupPath + "\\temp.txt");
                        System.IO.File.Copy(import_file, Application.StartupPath + "\\temp.txt");
                        CTK_parseTXTdata(MySQL, sql_db_table);
                        System.IO.File.Delete(import_file);
                    }
                }
                else
                {
                    LogDebug("Creating import directory", "IMPORT");
                    System.IO.Directory.CreateDirectory(Application.StartupPath + "\\import");
                }

            }
            catch (Exception ex)
            {
                lbl_LastError.Text = ex.Message.ToString();

                if (ex.Message.ToString() == "*.TXT: No such file or directory.")
                {
                    LogDebug("No files to proccess...", "FTP");
                }
                else
                {
                    LogDebug(ex.ToString(), "ERROR");
                }

            }

            // TRAFFIC
            if (LastDownloadedTraffic.AddMinutes(5) < DateTime.Now)
            {
                DoUpdateTraffic(MySQL, sql_db_table);
            }

            // CACHE
            if (LastCacheUpdate.AddMinutes(10) < DateTime.Now)
            {
                LogDebug("Building CACHE...", "SYSTEM");

                try
                {
                    sql_cmd = new MySQLCommand("truncate table " + sql_db_table + "_cache", MySQL);
                    sql_reader = sql_cmd.ExecuteReaderEx();
                }
                catch { }
                
                CACHE_build("CustNews_oblasti", sql_db_table, "SELECT DISTINCT oblast FROM " + sql_db_table + "_custnews_oblasti ORDER BY oblast ASC");
                CACHE_build("RIS_oblast", sql_db_table, "SELECT DISTINCT oblast FROM " + sql_db_table + "_ris_oblasti ORDER BY oblast ASC");
                
                CACHE_build("CustNews_mesto", sql_db_table, "SELECT DISTINCT cn_mesto FROM " + sql_db_table + "_custnews ORDER BY cn_mesto ASC");
                CACHE_build("RIS_mesto", sql_db_table, "SELECT DISTINCT ris_mesto FROM " + sql_db_table + "_ris ORDER BY ris_mesto ASC");

                CACHE_build("CustNews_key_0", sql_db_table, "SELECT DISTINCT(cn_klicova_slova) FROM " + sql_db_table + "_custnews WHERE (id!=0 OR base_id!=0)");
                CACHE_build("RIS_key_0", sql_db_table, "SELECT DISTINCT(ris_klicova_slova) FROM " + sql_db_table + "_ris WHERE (id!=0 OR base_id!=0)");

                LastCacheUpdate = DateTime.Now;
            }

            // DB PRUNE
            if (LastDBcleanup.AddHours(24) < DateTime.Now)
            {
                LogDebug("Deleting old records from DB", "SYSTEM");
                try
                {
                    DateTime t_del_6m = DateTime.Now.AddMonths(-6);
                    string del_6m = t_del_6m.Year.ToString("0000") + t_del_6m.Month.ToString("00") + t_del_6m.Day.ToString("00");
                    DateTime t_del_1m = DateTime.Now.AddMonths(-1);
                    string del_1m = t_del_1m.Year.ToString("0000") + t_del_1m.Month.ToString("00") + t_del_1m.Day.ToString("00");


                    sql_query = "DELETE FROM  " + sql_db_table + " WHERE datum<'" + del_6m + "'";
                    LogDebug(sql_query.ToString(), "SQL");
                    sql_cmd = new MySQLCommand(sql_query, MySQL);
                    sql_cmd.ExecuteNonQuery();

                    sql_query = "DELETE FROM  " + sql_db_table + "_sms WHERE sms_date<'" + del_6m + "'";
                    LogDebug(sql_query.ToString(), "SQL");
                    sql_cmd = new MySQLCommand(sql_query, MySQL);
                    sql_cmd.ExecuteNonQuery();

                    sql_query = "DELETE FROM  " + sql_db_table + "_custnews WHERE cn_datum<'" + del_6m + "'";
                    LogDebug(sql_query.ToString(), "SQL");
                    sql_cmd = new MySQLCommand(sql_query, MySQL);
                    sql_cmd.ExecuteNonQuery();

                    // sql_query = "DELETE FROM  " + sql_db_table + "_ris WHERE datum_konec<'" + del_1m + "'";
                    sql_query = "DELETE FROM  " + sql_db_table + "_ris WHERE datum_konec<'" + del_6m + "'";
                    LogDebug(sql_query.ToString(), "SQL");
                    sql_cmd = new MySQLCommand(sql_query, MySQL);
                    sql_cmd.ExecuteNonQuery();


                    sql_query = "OPTIMIZE TABLE " + sql_db_table + ", " + sql_db_table + "_sms, " + sql_db_table + "_custnews, " + sql_db_table + "_ris";
                    LogDebug(sql_query.ToString(), "SQL");
                    sql_cmd = new MySQLCommand(sql_query, MySQL);
                    sql_cmd.ExecuteNonQuery();

                }
                catch { }
                LastDBcleanup = DateTime.Now;
            }

            // Disconnect - FTP, MySQL
            try { ftp.Quit(); } catch { }
            LogDebug("Disconnected", "FTP");
            
            try { MySQL.Close(); } catch { }
            LogDebug("Disconnected", "SQL");

            // Do ZIP archive
            if (archive_ZIP_lastRun.AddHours(12) < DateTime.Now)
            {
                CTK_ZIP_archive();
                archive_ZIP_lastRun = DateTime.Now;
            }

            // SendErrors
            S = ini.IniReadValue("ReportError", "FTP");
            if (S != "") FTP_MaxErrorCount = Convert.ToInt32(S);
            S = ini.IniReadValue("ReportError", "MySQL");
            if (S != "") MySQL_MaxErrorCount = Convert.ToInt32(S);
            if ((DateTime.Now.Hour > 8) && (DateTime.Now.Hour < 22))
            {
                S = ini.IniReadValue("ReportError", "Delay-day");
                if (S != "") MaxIntervalBetweenDatas = Convert.ToInt32(S);
            }
            else
            {
                S = ini.IniReadValue("ReportError", "Delay-night");
                if (S != "") MaxIntervalBetweenDatas = Convert.ToInt32(S);
            }
            

            if (MySQL_ErrorCount == MySQL_MaxErrorCount)
            {
                ErrorMailSend("databáze MySQL je nedostupná.", "Počet pokusů o připojení: " + MySQL_MaxErrorCount.ToString());
                LogDebug("MySQL error", "MAIL");
                ErrorReported = true;
            }
            if (FTP_ErrorCount == FTP_MaxErrorCount)
            {
                ErrorMailSend("FTP server ČTK je nedostupný.", "Počet pokusů o připojení: " + FTP_MaxErrorCount.ToString());
                LogDebug("FTP error", "MAIL");
                ErrorReported = true;
            }
            if (DateTime.Compare(LastDownloadedData, DateTime.Now.AddMinutes(-MaxIntervalBetweenDatas)) < 0)
            {
                LoginSuc = false;
                if (ErrorReported == false)
                {
                    ErrorMailSend("ze serveru ČTK nebyla již dlouho stažena žádná zpráva.", "Maximální doba mezi zprávami od ČTK je nastavena na \""+MaxIntervalBetweenDatas.ToString()+"\" minut. Pravděpodobně se jedná o chybu na straně ČTK.");
                    LogDebug("too long without new data", "MAIL");
                    ErrorReported = true;
                }
            }
                // VSE OK.
            if ((ErrorReported == true) && (LoginSuc == true))
            {
                ErrorMailSend("ČTK server OK.", "Právě se podařilo stáhnout zprávu ze serveru ČTK - vše již bude asi OK.");
                LogDebug("ALL OK", "MAIL");
                ErrorReported = false;
            }



            // SetTimer
            if (StartTimer == true)
            {
                S = ini.IniReadValue("CTK", "refresh");
                try { I = Convert.ToInt32(S); } catch { }
                if (I >= 60) { ctk_refresh = I; }
                timer_DownloadCtkNews.Interval = (ctk_refresh * 1000);
                timer_DownloadCtkNews.Enabled = true;
                ctk_progress.Value = 0;
                I = (ctk_refresh / 1);
                if (I > 1)
                {
                    ctk_progress.Maximum = I;
                }
                else
                {
                    ctk_progress.Maximum = 100;
                }
                timer_wait.Interval = 1000;
                timer_wait.Enabled = true;
                LogDebug("Wait for next event (wait for " + ctk_refresh + " seconds)", "SYSTEM");
                lbl_status.Text = "Čekám na zahájení dalšího stahování (za " + ctk_refresh + " sekund)";
            }
            btn_DownloadCtkNews.Enabled = true;
            btn_stop.Enabled = true;
            UpdateStatusBar(MySQL_MaxErrorCount, FTP_MaxErrorCount, MaxIntervalBetweenDatas);
            WriteAppLog();
        }

        private void CACHE_build(string cache_title, string sql_db_table, string sql_query)
        {
            MySQLCommand sql_cmd, sql_cmd2;
            MySQLDataReader sql_reader;

            LogDebug(cache_title, "CACHE");
            // LogDebug(sql_query, "SQL");
            try
            {
                sql_cmd = new MySQLCommand(sql_query, MySQL);
                sql_reader = sql_cmd.ExecuteReaderEx();
                while (sql_reader.Read())
                {
                    sql_query = "INSERT INTO " + sql_db_table + "_cache SET c_key='"+cache_title+"', c_value='" + sql_reader[0].ToString() + "'";
                    sql_cmd2 = new MySQLCommand(sql_query, MySQL);
                    sql_cmd2.ExecuteNonQuery();
                }
            }
            catch { LogDebug(cache_title + " FAILED", "ERROR"); }
        }

        private void DoUpdateTraffic(MySQLConnection MySQL, string sql_db_table)
        {
            string html_doc = "";
            string TrafficTable = sql_db_table + "_traffic";
            string Data_TimeStamp = DateTime.Now.Day.ToString("00") + "." + DateTime.Now.Month.ToString("00") + "." + DateTime.Now.Year.ToString("0000") + " " + DateTime.Now.Hour.ToString("00") + ":" + DateTime.Now.Minute.ToString("00");
                
            string TrafficID = "";
            string TrafficURL = "";

            IFormatter formatter = new BinaryFormatter();
            object TimeStamp = DateTime.Now;
            string Data_TimeStamp_bin = "";
            bool noConnection = false;

            try
            {
                MemoryStream TimeStamp_MS = new MemoryStream();
                formatter.Serialize(TimeStamp_MS, TimeStamp);
                byte[] memory_array = TimeStamp_MS.ToArray();
                Data_TimeStamp_bin = Convert.ToBase64String(memory_array, Base64FormattingOptions.InsertLineBreaks);
                TimeStamp_MS.Close();
            }
            catch { }


            try
            {
                LogDebug("Updating TRAFFIC stupne", "TRAFFIC");
                try
                {
                    html_doc = Update_Traffic_Stupne();
                }
                catch { noConnection = true; }
                new MySQLDeleteCommand(MySQL, TrafficTable, new object[,] { { "id", "=", "stupne" } }, null);
                new MySQLInsertCommand(MySQL, new object[,] { { "id", "stupne" }, { "html_data", html_doc }, { "data_update", Data_TimeStamp }, { "data_update_bin", Data_TimeStamp_bin } }, TrafficTable);

                LastDownloadedTraffic = DateTime.Now;
            }
            catch (Exception e) { LogDebug(e.ToString(), "TRAFFIC"); }

            try
            {
                string sql_query = "SELECT id, url FROM " + TrafficTable + "_url WHERE url != ''";
                MySQLCommand sql_cmd = new MySQLCommand(sql_query, MySQL);
                MySQLDataReader sql_reader = sql_cmd.ExecuteReaderEx();
                while (sql_reader.Read())
                {
                    try
                    {
                        TrafficID = sql_reader.GetString(0);
                        TrafficURL = sql_reader.GetString(1);

                        if (noConnection != true)
                        {
                            LogDebug("Updating TRAFFIC omezeni - " + TrafficID, "TRAFFIC");
                            try
                            {
                                html_doc = Update_Traffic_Nehody(TrafficURL);
                            }
                            catch { noConnection = true; }
                            new MySQLDeleteCommand(MySQL, TrafficTable, new object[,] { { "id", "=", TrafficID } }, null);
                            new MySQLInsertCommand(MySQL, new object[,] { { "id", TrafficID }, { "html_data", html_doc }, { "data_update", Data_TimeStamp }, { "data_update_bin", Data_TimeStamp_bin } }, TrafficTable);

                            LastDownloadedTraffic = DateTime.Now;
                        }
                    }
                    catch (Exception e) { LogDebug(e.ToString(), "TRAFFIC"); }
                }
            }
            catch { }

            app_status_traffic.Text = LastDownloadedTraffic.ToShortTimeString();
        }

        public string XML_XSL_to_HTML(string xml, string xsl)
        {
            string return_html = "";
            try
            {
                // Load the XML string into an XPathDocument.
                StringReader xmlStringReader = new StringReader(xml);
                XPathDocument xPathDocument = new XPathDocument(xmlStringReader);

                // Create a reader to read the XSL.
                StringReader xslStringReader = new StringReader(xsl);
                XmlTextReader xslTextReader = new XmlTextReader(xslStringReader);

                // Load the XSL into an XslTransform.
                XslTransform xslTransform = new XslTransform();
                xslTransform.Load(xslTextReader, null, GetType().Assembly.Evidence);

                // Perform the actual transformation and output an HTML string.
                StringWriter htmlStringWriter = new StringWriter();
                xslTransform.Transform(xPathDocument, null, htmlStringWriter, null);
                return_html = htmlStringWriter.ToString();

                // Close all our readers and writers.
                xmlStringReader.Close();
                xslStringReader.Close();
                xslTextReader.Close();
                htmlStringWriter.Close();

            }
            catch
            {
                return_html = "<html><head></head><body style=\"background-color: #BBD9FF; font-weight: bold;\">Chyba pĹ™i zpracovĂˇnĂ­ dat.</body></html>";
            }
            // Done, return the created HTML code.
            return return_html;
        }

        private string HtmlRewriteDopravaDegree(string html)
        {
            html = html.Replace("#DOPRAVA-DEGREE-1#", "<div style=\"background-color: #00CC33;\">");
            html = html.Replace("#DOPRAVA-DEGREE-2#", "<div style=\"background-color: #CCFF66;\">");
            html = html.Replace("#DOPRAVA-DEGREE-3#", "<div style=\"background-color: #FFFF00;\">");
            html = html.Replace("#DOPRAVA-DEGREE-4#", "<div style=\"background-color: #FF9900;\">");
            html = html.Replace("#DOPRAVA-DEGREE-5#", "<div style=\"background-color: #FF3300;\">");
            html = html.Replace("#DOPRAVA-DEGREE-END#", "</div>");

            return (html);
        }

        private string Update_Traffic_Stupne()
        {
            string doprava_url_stupne = "http://www.aba.cz/rss/stupne_xml.php/bcmedia";

            LogDebug("Update_Traffic_Stupne()", "TRAFFIC");

            string html_doc = "";
            string xml_doc = "";
            string xsl_doc = "";
            WebClient client = new WebClient();

            xsl_doc = Properties.Resources.xsl_doprava_stupne;
            client.Encoding = System.Text.Encoding.UTF8;
            xml_doc = client.DownloadString(doprava_url_stupne);
            html_doc = XML_XSL_to_HTML(xml_doc, xsl_doc);
            html_doc = HtmlRewriteDopravaDegree(html_doc);
            return (html_doc);
        }

        private string Update_Traffic_Nehody(string doprava_url_nehody)
        {
            string html_doc = "";
            string xml_doc = "";
            string xsl_doc = "";
            WebClient client = new WebClient();

            xsl_doc = Properties.Resources.xsl_doprava_nehody;
            client.Encoding = System.Text.Encoding.UTF8;
            xml_doc = client.DownloadString(doprava_url_nehody);
            html_doc = XML_XSL_to_HTML(xml_doc, xsl_doc);
            
            return(html_doc);
        }


        protected void UpdateStatusBar(int MySQL_MaxErrorCount, int FTP_MaxErrorCount, int MaxIntervalBetweenDatas)
        {
            app_status_lastdownload.Text = LastDownloadedData.ToShortTimeString();
            if (DateTime.Compare(LastDownloadedData, DateTime.Now.AddMinutes(-MaxIntervalBetweenDatas)) < 0){
                app_status_lastdownload.ForeColor = Color.Red;
            } else { app_status_lastdownload.ForeColor = Color.DarkGreen; }

            app_status_FTP_error.Text = FTP_ErrorCount.ToString();
            if (FTP_ErrorCount >= FTP_MaxErrorCount) { app_status_FTP_error.ForeColor = Color.Red; }
            else { app_status_FTP_error.ForeColor = Color.DarkGreen; }

            app_status_MySQL_error.Text = MySQL_ErrorCount.ToString();
            if (MySQL_ErrorCount >= MySQL_MaxErrorCount) { app_status_MySQL_error.ForeColor = Color.Red; }
            else { app_status_MySQL_error.ForeColor = Color.DarkGreen; }
        }

        private void CTK_ZIP_archive()
        {
            LogDebug("Looking for dirs", "ZIP");
            try
            {
                string archive_path = Application.StartupPath + "\\archive\\" + System.DateTime.Today.Year.ToString() + "\\" + System.DateTime.Today.Month.ToString("0#") + "\\" + System.DateTime.Today.Day.ToString("0#") + "\\";
                string archive_base_path = Application.StartupPath + "\\archive\\";
                string ZIP_name = null;
                string[] archive_days_files = null;

                string[] archive_years = Directory.GetDirectories(archive_base_path);
                foreach (string archive_years_dir in archive_years)
                {
                    string[] archive_months = Directory.GetDirectories(archive_years_dir);
                    foreach (string archive_months_dir in archive_months)
                    {
                        string[] archive_days = Directory.GetDirectories(archive_months_dir);
                        foreach (string archive_days_dir in archive_days)
                        {
                            if (archive_path != archive_days_dir + "\\")
                            {
                                LogDebug("Compressing to ZIP: " + archive_days_dir.Replace(Application.StartupPath, ""), "ZIP");
                                try
                                {
                                    archive_days_files = Directory.GetFiles(archive_days_dir);
                                    ZIP_name = archive_days_dir + ".zip";
                                    ZipOutputStream s = new ZipOutputStream(File.Create(ZIP_name));
                                    s.SetLevel(9); // 0 - store only to 9 - means best compression

                                    foreach (string file in archive_days_files)
                                    {
                                        FileStream fs = File.OpenRead(file);
                                        byte[] buffer = new byte[fs.Length];
                                        fs.Read(buffer, 0, buffer.Length);

                                        ZipEntry entry = new ZipEntry(file.Replace(archive_days_dir + "\\", ""));

                                        s.PutNextEntry(entry);
                                        s.Write(buffer, 0, buffer.Length);
                                        fs.Close();
                                    }

                                    s.Finish();
                                    s.Close();
                                    
                                    Application.DoEvents();
                                    System.IO.Directory.Delete(archive_days_dir, true);
                                }
                                catch (Exception ex)
                                {
                                    LogDebug("Error compressing file.\r\n" + ex.ToString(), "ZIP");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LogDebug("Unknown error.\r\n" + e.ToString(), "ZIP");
            }

        }
        
        private void CTK_TXT_archive(string[] files, int i)
        {
            // ARCHIVUJ...
            string archive_path = Application.StartupPath + "\\archive\\" + System.DateTime.Today.Year.ToString() + "\\" + System.DateTime.Today.Month.ToString("0#") + "\\" + System.DateTime.Today.Day.ToString("0#") + "\\";
            try
            {
                if (System.IO.Directory.Exists(archive_path) == false)
                {
                    System.IO.Directory.CreateDirectory(archive_path);
                }
            }
            catch
            {
                LogDebug("can't create archive directory (" + archive_path + ").", "ERROR");
            }

            try
            {
                System.IO.File.Copy(Application.StartupPath + "\\temp.txt", archive_path + files[i]);
                LogDebug(files[i] + " archived sucesfully.", "ARCHIVE");
            }
            catch
            {
                LogDebug("can't copy downloaded file to archive directory (" + files[i] + ").", "ERROR");
            }
        }

        private void CTK_parseTXTdata(MySQLConnection MySQL, string sql_db_table)
        {
            string S;
            string ctk_datum = null;
            string ctk_cas = null;
            string ctk_kategorie = null;
            string ctk_titulek = null;
            string ctk_text = null;
            string ctk_klicova_slova = null;
            string ctk_spec_kat = null;
            string ctk_kraj = null;
            StreamReader CTK_TXT_file;

            CTK_TXT_file = new StreamReader(Application.StartupPath + "\\temp.txt", System.Text.Encoding.GetEncoding("windows-1250"));
            S = CTK_TXT_file.ReadLine();
            try
            {
                string[] CTK_Head = S.Split('\t');
                ctk_datum = CTK_Head[5].Trim();
                ctk_cas = CTK_Head[6].Trim();
                ctk_kategorie = CTK_Head[3].Trim();
                ctk_spec_kat = CTK_Head[0].Trim().Substring(0, 1);
                ctk_kraj = CTK_Head[0].Trim().Substring(1, 2);

                ctk_kategorie = "!" + ctk_spec_kat + "! " + ctk_kategorie;
                ctk_kategorie += " ^" + ctk_kraj + "^";

                ctk_klicova_slova = CTK_TXT_file.ReadLine().Trim();
                ctk_klicova_slova = ctk_klicova_slova.Replace(';', '-');
                ctk_titulek = CTK_TXT_file.ReadLine().Trim();
                ctk_text = null;
                do
                {
                    S = CTK_TXT_file.ReadLine();
                    ctk_text += S + "\r\n";
                } while (S != null);

                new MySQLInsertCommand(MySQL, new object[,] { { "datum", ctk_datum }, { "cas", ctk_cas }, { "kategorie", ctk_kategorie }, { "titulek", ctk_titulek }, { "text", ctk_text }, { "klicova_slova", ctk_klicova_slova } }, sql_db_table);
                LastDownloadedData = DateTime.Now;
            }
            catch
            {
                LogDebug("empty file!!!", "ERROR");
                ErrorMailSend("Soubor neobsahuje žádná data!", "");
            }

            CTK_TXT_file.Close();
        }

        void btn_DownloadCtkNews_click(object sender, System.EventArgs e)
        {
            DownloadCtkNews();
        }
        void timer_DownloadCtkNews_tick(object sender, System.EventArgs e)
        {
            DownloadCtkNews();
        }
        void btn_cfg_click(object sender, System.EventArgs e)
        {
            //TODO: Open Config
            cfg cfg_wnd = new cfg(this);
            cfg_wnd.Show();
        }

        void timer_wait_tick(object sender, System.EventArgs e)
        {
            try
            {
                ctk_progress.Value++;
            }
            catch
            {
                LogDebug("Value is greater then maximum!", "ctk_progress");
            }
        }

        void btn_stop_click(object sender, System.EventArgs e)
        {
            timer_wait.Enabled = false;
            timer_DownloadCtkNews.Enabled = false;
            ctk_progress.Value = 0;
            lbl_status.Text = "SERVER ZASTAVEN!";
            LogDebug("SERVER STOP", "USER");
        }

    }
}
